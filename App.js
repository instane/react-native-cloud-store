import React from "react";
import { Provider } from "react-redux";
import {
  Provider as PaperProvider,
  DarkTheme,
  DefaultTheme
} from "react-native-paper";
import RootStack from "./src/navigation";
import store from "./src/state";

// variants: DarkTheme || DefaultTheme
const theme = DarkTheme;
theme.colors.primary = "#776ead";
// theme.roundness = 5

export default (App = () => {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <RootStack />
      </PaperProvider>
    </Provider>
  );
});

// export default (class App extends React.PureComponent {
//   render() {
//     return (
//       <Provider store={store}>
//         <PaperProvider theme={theme}>
//           <RootStack />
//         </PaperProvider>
//       </Provider>
//     );
//   }
// });
