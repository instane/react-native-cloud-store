import thunkMiddleware from "redux-thunk";
// import { createLogger } from "redux-logger";
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";

// const loggerMiddleware = createLogger();

export default (store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware, // позволяет нам отправлять функции
    // loggerMiddleware // аккуратно логируем действия
  )
));
