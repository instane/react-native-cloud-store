// import firebase from "react-native-firebase";
import { app as firebase, hookahRef, tobaccoRef, newsRef } from "../firebase";

/*
 * типы действий
 */

export const SET_COUNTRY_CODE = "SET_COUNTRY_CODE";
export const SET_PHONE = "SET_PHONE";
export const RESET_PHONE = "RESET_PHONE";
export const CODE_REQUEST = "CODE_REQUEST";
export const CODE_SUCCESS = "CODE_SUCCESS";
export const CODE_FAILURE = "CODE_FAILURE";
export const SET_CONFIRM_RESULT = "SET_CONFIRM_RESULT";

export const SET_CODE = "SET_CODE";
export const RESET_CODE = "RESET_CODE";
export const CODE_CHECK_REQUEST = "CODE_CHECK_REQUEST";
export const CODE_CHECK_SUCCESS = "CODE_CHECK_SUCCESS";
export const CODE_CHECK_FAILURE = "CODE_CHECK_FAILURE";
export const SET_USER = "SET_USER";
export const SET_USER_NAME_INPUT = "SET_USER_NAME_INPUT";
export const RESET_USER = "RESET_USER";
export const RESET_CODE_ERRORS = "RESET_CODE_ERRORS";
export const CODE_HAS_ERRORS = "CODE_HAS_ERRORS";

export const USER_REQUEST = "USER_REQUEST";
export const USER_SUCCESS = "USER_SUCCESS";
export const USER_FAILURE = "USER_FAILURE";

export const SET_FCM_TOKEN = "SET_FCM_TOKEN";

export const PHONE_HAS_ERRORS = "PHONE_HAS_ERRORS";
export const RESET_PHONE_ERRORS = "RESET_PHONE_ERRORS";

export const SET_HOOKAH_LIST = "SET_HOOKAH_LIST";
export const HOOKAH_REQUEST = "HOOKAH_REQUEST";
export const HOOKAH_SUCCESS = "HOOKAH_SUCCESS";
export const HOOKAH_FAILURE = "HOOKAH_FAILURE";

export const TOBACCO_REQUEST = "TOBACCO_REQUEST";
export const TOBACCO_SUCCESS = "TOBACCO_SUCCESS";
export const TOBACCO_FAILURE = "TOBACCO_FAILURE";
export const SET_TOBACCO_LIST = "SET_TOBACCO_LIST";

export const SET_NEWS_LIST = "SET_NEWS_LIST";
export const NEWS_REQUEST = "NEWS_REQUEST";
export const NEWS_SUCCESS = "NEWS_SUCCESS";
export const NEWS_FAILURE = "NEWS_FAILURE";
/*
 * генераторы действий
 */

export function codeHasErrors() {
  return { type: CODE_HAS_ERRORS };
}

export function resetCodeErrors() {
  return { type: RESET_CODE_ERRORS };
}

export function phoneHasErrors() {
  return { type: PHONE_HAS_ERRORS };
}

export function resetPhoneErrors() {
  return { type: RESET_PHONE_ERRORS };
}

export function setCountryCode(code) {
  return { type: SET_COUNTRY_CODE, code };
}
export function setPhone(phone) {
  return { type: SET_PHONE, phone };
}
export function resetPhone() {
  return { type: RESET_PHONE };
}
export function codeRequest(phone) {
  return { type: CODE_REQUEST, phone };
}
export function codeSuccess() {
  return { type: CODE_SUCCESS };
}
export function codeFailure(error) {
  return { type: CODE_FAILURE, error };
}
export function setConfirmResult(confirmResult) {
  return { type: SET_CONFIRM_RESULT, confirmResult };
}

export function fetchConfirmResult(phone) {
  return dispatch => {
    dispatch(codeRequest(phone));

    return firebase
      .auth()
      .signInWithPhoneNumber(phone)
      .then(confirmResult => {
        dispatch(setConfirmResult(confirmResult));
        dispatch(codeSuccess());
        return confirmResult;
      })
      .catch(error => {
        dispatch(codeFailure(error));
        dispatch(setMessage(error.message));
        dispatch(phoneHasErrors());
        throw error;
      });
  };
}

export function setCode(code) {
  return { type: SET_CODE, code };
}
export function resetCode() {
  return { type: RESET_CODE };
}
export function codeCheckRequest(code) {
  return { type: CODE_CHECK_REQUEST, code };
}
export function codeCheckSuccess() {
  return { type: CODE_CHECK_SUCCESS };
}
export function codeCheckFailure(error) {
  return { type: CODE_CHECK_FAILURE, error };
}
export function setUser(user) {
  return { type: SET_USER, user };
}
export function setUserNameInput(name) {
  return { type: SET_USER_NAME_INPUT, name };
}
export function setUserName(user, displayName) {
  return dispatch => {
    dispatch(userRequest());

    if (!user) {
      return new Promise((resolve, reject) => {
        throw { message: "You have not signed in" };
      });
    }

    // displayName = displayName ? displayName : "Anonymous";

    return user
      .updateProfile({ displayName })
      .then(() => {
        dispatch(setUser(firebase.auth().currentUser));
        dispatch(codeCheckSuccess());
        return user;
      })
      .catch(error => {
        dispatch(userFailure(error));
        dispatch(setMessage(error.message));
        throw error;
      });
  };
}
export function userRequest() {
  return { type: USER_REQUEST };
}
export function userSuccess() {
  return { type: USER_SUCCESS };
}
export function userFailure() {
  return { type: USER_FAILURE };
}
export function resetUser() {
  return dispatch => {
    dispatch(userRequest());

    return firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch(setUser(null));
        dispatch(userSuccess());
      })
      .catch(error => {
        dispatch(userFailure());
        dispatch(setMessage(error.message));
        throw error;
      });
  };
}

export function fetchUser(confirmResult, code) {
  return dispatch => {
    dispatch(codeCheckRequest(code));

    if (!confirmResult) {
      return new Promise((resolve, reject) => {
        throw { message: "You have not submit phone number" };
      });
    }

    return confirmResult
      .confirm(code)
      .then(user => {
        dispatch(setUser(user));
        dispatch(codeCheckSuccess());
        return user;
      })
      .catch(error => {
        dispatch(codeCheckFailure(error));
        dispatch(setMessage(error.message));
        dispatch(codeHasErrors());
        throw error;
      });
  };
}

export function fetchFcmToken() {
  return dispatch => {
    dispatch(userRequest());

    return firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          dispatch(userSuccess());
          dispatch(setFcmToken(fcmToken));
        } else {
          // console.log("user doesn't have a device token yet");
        }
      })
      .catch(error => {
        dispatch(userFailure());
      });
  };
}

export function setFcmToken(token) {
  return { type: SET_FCM_TOKEN, token };
}

export function hookahRequest() {
  return { type: HOOKAH_REQUEST };
}
export function hookahSuccess() {
  return { type: HOOKAH_SUCCESS };
}
export function hookahFailure() {
  return { type: HOOKAH_FAILURE };
}

export function setHookahList(list) {
  return { type: SET_HOOKAH_LIST, list };
}

export function fetchHookas() {
  return dispatch => {
    dispatch(hookahRequest());

    // firebase.firestore().setFirestoreSettings({persistence: false})
    firebase.firestore().enableNetwork() 

    return hookahRef
      .get()
      .then(querySnapshot => {
        const list = [];
        querySnapshot.forEach(doc => {
          const data = doc.data();
          list.push({
            key: doc.id,
            ...data
          });
        });
        dispatch(setHookahList(list));
        dispatch(hookahSuccess());
      })
      .catch(error => {
        dispatch(hookahFailure());
        dispatch(setMessage(error.message));
        throw error;
      });
  };
}

export function tobaccoRequest() {
  return { type: TOBACCO_REQUEST };
}
export function tobaccoSuccess() {
  return { type: TOBACCO_SUCCESS };
}
export function tobaccoFailure() {
  return { type: TOBACCO_FAILURE };
}

export function setTobaccoList(list) {
  return { type: SET_TOBACCO_LIST, list };
}

export function fetchTobaccos() {
  return dispatch => {
    dispatch(tobaccoRequest());

    firebase.firestore().enableNetwork() 

    return tobaccoRef
      .get()
      .then(querySnapshot => {
        const list = [];
        querySnapshot.forEach(doc => {
          const data = doc.data();
          list.push({
            key: doc.id,
            ...data
          });
        });
        dispatch(setTobaccoList(list));
        dispatch(tobaccoSuccess());
      })
      .catch(error => {
        dispatch(tobaccoFailure());
        dispatch(setMessage(error.message));
        throw error;
      });
  };
}

export function newsRequest() {
  return { type: NEWS_REQUEST };
}
export function newsSuccess() {
  return { type: NEWS_SUCCESS };
}
export function newsFailure() {
  return { type: NEWS_FAILURE };
}
export function setNewsList(list) {
  return { type: SET_NEWS_LIST, list };
}

export function fetchNews() {
  return dispatch => {
    dispatch(newsRequest());

    firebase.firestore().enableNetwork() 

    return newsRef
      .get()
      .then(querySnapshot => {
        const list = [];
        querySnapshot.forEach(doc => {
          const data = doc.data();
          list.push({
            key: doc.id,
            ...data
          });
        });
        dispatch(setNewsList(list));
        dispatch(newsSuccess());
      })
      .catch(error => {
        dispatch(newsFailure());
        dispatch(setMessage(error.message));
        throw error;
      });
  };
}