import { combineReducers } from "redux";
import {
  USER_REQUEST,
  USER_SUCCESS,
  USER_FAILURE,
  SET_PHONE,
  SET_COUNTRY_CODE,
  RESET_PHONE,
  CODE_REQUEST,
  CODE_SUCCESS,
  CODE_FAILURE,
  SET_CODE,
  RESET_CODE,
  CODE_CHECK_REQUEST,
  CODE_CHECK_SUCCESS,
  CODE_CHECK_FAILURE,
  SET_CONFIRM_RESULT,
  SET_USER,
  SET_USER_NAME_INPUT,
  RESET_USER,
  SET_FCM_TOKEN,
  RESET_CODE_ERRORS,
  CODE_HAS_ERRORS,
  PHONE_HAS_ERRORS,
  RESET_PHONE_ERRORS,
  SET_HOOKAH_LIST,
  HOOKAH_REQUEST,
  HOOKAH_SUCCESS,
  HOOKAH_FAILURE,
  SET_TOBACCO_LIST,
  TOBACCO_REQUEST,
  TOBACCO_SUCCESS,
  TOBACCO_FAILURE,
  SET_NEWS_LIST,
  NEWS_REQUEST,
  NEWS_SUCCESS,
  NEWS_FAILURE
} from "./actions";

const defaultCodeCheckState = {
  codeInput: "",
  confirmResult: null,
  isFetching: false,
  hasErrors: false
};
function codeCheck(state = defaultCodeCheckState, action) {
  switch (action.type) {
    case RESET_CODE_ERRORS:
      return { ...state, hasErrors: false };
    case CODE_HAS_ERRORS:
      return { ...state, hasErrors: true };
    case SET_CODE:
      return { ...state, codeInput: action.code };
    case RESET_CODE:
      return { ...state, codeInput: defaultCodeCheckState.codeInput };
    case SET_CONFIRM_RESULT:
      return { ...state, confirmResult: action.confirmResult };
    case CODE_CHECK_REQUEST:
      return { ...state, isFetching: true };
    case CODE_CHECK_SUCCESS:
      return {
        ...state,
        isFetching: false
      };
    case CODE_CHECK_FAILURE:
      return { ...state, isFetching: false };
    default:
      return state;
  }
}

const defaultCodeRequestState = {
  countryCode: "7",
  phoneNumber: "",
  isFetching: false,
  hasErrors: false
};
function codeRequest(state = defaultCodeRequestState, action) {
  switch (action.type) {
    case RESET_PHONE_ERRORS:
      return { ...state, hasErrors: false };
    case PHONE_HAS_ERRORS:
      return { ...state, hasErrors: true };
    case RESET_PHONE:
      return { ...state, phoneNumber: defaultCodeRequestState.phoneNumber };
    case SET_COUNTRY_CODE:
      return { ...state, countryCode: action.code };
    case SET_PHONE:
      return { ...state, phoneNumber: action.phone };
    case CODE_REQUEST:
      return { ...state, isFetching: true };
    case CODE_SUCCESS:
      return {
        ...state,
        isFetching: false
      };
    case CODE_FAILURE:
      return { ...state, isFetching: false };
    default:
      return state;
  }
}

const defaultUserState = {
  user: null,
  unsubscribe: null,
  isFetching: false,
  fcmToken: "",
  nameInput: ""
};
function user(state = defaultUserState, action) {
  switch (action.type) {
    case SET_USER_NAME_INPUT:
      return { ...state, nameInput: action.name };
    case SET_FCM_TOKEN:
      return { ...state, fcmToken: action.token };
    case RESET_USER:
      return { ...state, user: defaultUserState.user };
    case SET_USER:
      return { ...state, user: action.user };
    case USER_REQUEST:
      return { ...state, isFetching: true };
    case USER_SUCCESS:
      return {
        ...state,
        isFetching: false
      };
    case USER_FAILURE:
      return { ...state, isFetching: false };
    default:
      return state;
  }
}

const defaultHookahState = {
  list: [],
  isFetching: false
};
function hookah(state = defaultHookahState, action) {
  switch (action.type) {
    case SET_HOOKAH_LIST:
      return { ...state, list: action.list };
    case HOOKAH_REQUEST:
      return { ...state, isFetching: true };
    case HOOKAH_SUCCESS:
      return { ...state, isFetching: false };
    case HOOKAH_FAILURE:
      return { ...state, isFetching: false };
    default:
      return state;
  }
}

const defaultTobaccoState = {
  list: [],
  isFetching: false
};
function tobacco(state = defaultTobaccoState, action) {
  switch (action.type) {
    case SET_TOBACCO_LIST:
      return { ...state, list: action.list };
    case TOBACCO_REQUEST:
      return { ...state, isFetching: true };
    case TOBACCO_SUCCESS:
      return { ...state, isFetching: false };
    case TOBACCO_FAILURE:
      return { ...state, isFetching: false };
    default:
      return state;
  }
}

const defaultNewsState = {
  list: [],
  isFetching: false
};
function news(state = defaultNewsState, action) {
  switch (action.type) {
    case SET_NEWS_LIST:
      return { ...state, list: action.list };
    case NEWS_REQUEST:
      return { ...state, isFetching: true };
    case NEWS_SUCCESS:
      return { ...state, isFetching: false };
    case NEWS_FAILURE:
      return { ...state, isFetching: false };
    default:
      return state;
  }
}

const app = combineReducers({
  codeRequest,
  codeCheck,
  user,
  hookah,
  tobacco,
  news
});

export default app;
