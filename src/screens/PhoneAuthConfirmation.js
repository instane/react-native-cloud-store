import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import { View } from "react-native";
import { TextInput, HelperText, withTheme } from "react-native-paper";
// import Message from "../components/Message";
import ForwardFAB from "../components/ForwardFAB";

class PhoneAuthConfirmationDefault extends PureComponent {
  confirmCode = () => {
    const { confirmResult, codeInput } = this.props.codeCheck;

    if (confirmResult && codeInput.length) {
      this.props.actions
        .fetchUser(confirmResult, codeInput)
        // .then(user => this.props.actions.setMessage("Code Confirmed!"))
        .then(() => this.props.navigation.navigate("AuthLoading"))
        // .catch(error =>
        //   this.props.actions.setMessage(`Code Confirm Error: ${error.message}`)
        // );
    }
  };

  verificationCodeInputChange = value => {
    this.props.actions.setCode(value);
    this.props.actions.resetCodeErrors();
  };

  renderVerificationCodeInput() {
    const { codeInput, hasErrors } = this.props.codeCheck;

    return (
      <View style={{ marginTop: 40, padding: 20 }}>
        <TextInput
          autoFocus
          onChangeText={value => this.verificationCodeInputChange(value)}
          onSubmitEditing={this.confirmCode}
          // label={"Code"}
          label={"Код"}
          value={codeInput}
          keyboardType={"numeric"}
          error={hasErrors}
        />
        <HelperText type="error" visible={hasErrors}>
          {/* Invalid code, please try again. */}
          Неправильный код, пожалуйста попробуйте ещё раз.
        </HelperText>
      </View>
    );
  }

  render() {
    const { user } = this.props.user;
    // const { message } = this.props.message;
    const { confirmResult, codeInput, isFetching } = this.props.codeCheck;
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: background }}>
        {!user && confirmResult && this.renderVerificationCodeInput()}
        {/* {__DEV__ && <Message message={message} />} */}
        {codeInput.length > 0 &&
          confirmResult && (
            <ForwardFAB loading={isFetching} onPress={this.confirmCode} />
          )}
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    // message: state.message,
    user: state.user,
    codeRequest: state.codeRequest,
    codeCheck: state.codeCheck
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PhoneAuthConfirmationDefault)
);
