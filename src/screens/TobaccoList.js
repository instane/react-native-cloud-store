import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Card,
  CardContent,
  CardCover,
  Title,
  Paragraph,
  withTheme
} from "react-native-paper";
import { tobaccoRef } from "../firebase";

class TobaccoList extends PureComponent {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
  }

  componentDidMount() {
    this.unsubscribe = tobaccoRef.onSnapshot(this.onCollectionUpdate);
  }

  onCollectionUpdate = querySnapshot => {
    const list = [];
    querySnapshot.forEach(doc => {
      const data = doc.data();
      list.push({
        key: doc.id,
        ...data
      });
    });
    this.props.actions.setTobaccoList(list);
  };

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  cardPressed = tobacco => {
    this.props.navigation.navigate("TobaccoItem", { tobacco });
  };

  tobaccoCard = tobacco => {
    const { key, name, pictureUrl } = tobacco;
    return (
      <Card key={key}>
        <CardContent>
          <Title>{name}</Title>
          <Paragraph />
        </CardContent>
        <TouchableOpacity onPress={() => this.cardPressed(tobacco)}>
          <CardCover source={{ uri: pictureUrl }} />
        </TouchableOpacity>
      </Card>
    );
  };

  render() {
    const { list, isFetching } = this.props.tobacco;
    const listItems = list.map(tobacco => this.tobaccoCard(tobacco));
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    return (
      <ScrollView style={{ backgroundColor: background }}>
        <View style={styles.container}>
          {isFetching && <ActivityIndicator />}
          {listItems}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  defaultView: {
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#77dd77",
    flex: 1
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    tobacco: state.tobacco
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TobaccoList)
);
