import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import { StyleSheet, View } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import {
  app as firebase,
  notificationsChannelId,
  maxAndroidNotificationPriority
} from "../firebase";

class AuthLoadingScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
    this.notificationOpenedListener = null;
    this.notificationListener = null;
  }

  // Triggered when recieved notification in foreground
  onNotification = notification => {
    if (!notification.android.channelId) {
      notification.android.setChannelId(notificationsChannelId);
    }
    if (!notification.android.smallIcon) {
      notification.android.setSmallIcon("ic_launcher");
    }
    notification.android.setPriority(maxAndroidNotificationPriority);

    // console.log("onNotification");
    // console.log(notification);
    firebase.notifications().displayNotification(notification);
  };

  // Triggered when notification clicked in foreground
  onNotificationOpened = notification => {
    // console.log("onNotificationOpened");
    // console.log(notification);
    notification = notification.notification;

    firebase
      .notifications()
      .removeDeliveredNotification(notification.notificationId);
  };

  // Triggered when app opened by clicking on notification in background
  onInitialNotification = notification => {
    if (notification) {
      // console.log("onInitialNotification");
      // console.log(notification);
    }
  };

  requestMessagingPermission = () => {
    return firebase.messaging().requestPermission();
  };

  checkMessagingPermission = isEnabled => {
    if (!isEnabled) {
      return this.requestMessagingPermission();
    }
  };

  initializeNotifications = () => {
    firebase
      .messaging()
      .hasPermission()
      .then(isEnabled => this.checkMessagingPermission(isEnabled));

    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => this.onNotification(notification));

    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notification =>
        this.onNotificationOpened(notification)
      );

    firebase
      .notifications()
      .getInitialNotification()
      .then(notification => this.onInitialNotification(notification));
  };

  prefetchData = () => {
    this.props.actions.fetchHookas();
    this.props.actions.fetchTobaccos();
    this.props.actions.fetchNews();
  };

  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.props.actions.setUser(user);

        this.initializeNotifications();
        this.prefetchData();

        return this.props.navigation.navigate(
          user.displayName === null ? "ProfileInit" : "App"
        );
      }
      return this.props.navigation.navigate(user ? "App" : "Auth");
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  render() {
    const visible = true;
    return (
      <View style={styles.container}>
        <Spinner
          visible={visible}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthLoadingScreen);
