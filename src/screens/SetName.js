import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import { View } from "react-native";
import { TextInput, HelperText, withTheme } from "react-native-paper";
// import Message from "../components/Message";
import ForwardFAB from "../components/ForwardFAB";

class SetName extends PureComponent {
  setName = () => {
    const { user, nameInput } = this.props.user;

    if (user) {
      this.props.actions
        .setUserName(user, nameInput)
        // .then(user => this.props.actions.setMessage("Name Set!"))
        .then(() => this.props.navigation.navigate("Home"))
        // .catch(error =>
        //   this.props.actions.setMessage(`Name Set Error: ${error.message}`)
        // );
    }
  };

  renderSetNameInput() {
    const { nameInput } = this.props.user;

    return (
      <View style={{ marginTop: 40, padding: 20 }}>
        <TextInput
          autoFocus
          onChangeText={value => this.props.actions.setUserNameInput(value)}
          onSubmitEditing={this.setName}
          // label={"Your Name"}
          label={"Имя"}
          value={nameInput}
          keyboardType={"default"}
        />
        <HelperText type="info">
          {/* This step can be skipped. */}
          Этот шаг может быть пропущен.
        </HelperText>
      </View>
    );
  }

  render() {
    const { user, isFetching } = this.props.user;
    // const { message } = this.props.message;
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: background }}>
        {user && this.renderSetNameInput()}
        {/* {__DEV__ && <Message message={message} />} */}
        <ForwardFAB loading={isFetching} onPress={this.setName} />
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    // message: state.message,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SetName)
);
