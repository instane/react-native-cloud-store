import React, { PureComponent, Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import { View } from "react-native";
import { TextInput, Text, HelperText, withTheme } from "react-native-paper";
// import Message from "../components/Message";
import ForwardFAB from "../components/ForwardFAB";

class PhoneAuthDefault extends PureComponent {
  signIn = () => {
    const { phoneNumber, countryCode } = this.props.codeRequest;
    const fullPhoneNumber = `+${countryCode}${phoneNumber}`;
    // this.props.actions.setMessage("Sending code ...");

    this.props.actions
      .fetchConfirmResult(fullPhoneNumber)
      // .then(confirmResult =>
      //   // this.props.actions.setMessage("Code has been sent!")
      // )
      .then(() => {
        this.props.actions.resetCode();
        this.props.actions.resetCodeErrors();
        this.props.navigation.navigate("PhoneAuthConfirmation");
      })
      // .catch(error =>
      //   this.props.actions.setMessage(
      //     `Sign In With Phone Number Error: ${error.message}`
      //   )
      // );
  };

  phoneNumberInputChange = value => {
    this.props.actions.setPhone(value);
    this.props.actions.resetPhoneErrors();
  };

  renderPhoneNumberInput() {
    const { phoneNumber, countryCode, hasErrors } = this.props.codeRequest;

    return (
      <View style={{ padding: 20, marginTop: 40 }}>
        <View
          flexDirection={"row"}
          alignItems={"flex-end"}
          justifyContent={"space-between"}
        >
          <View style={{ width: "20%" }} flexDirection={"row"}>
            <Text style={{ alignSelf: "center", marginTop: 5 }}>+</Text>
            <TextInput
              value={countryCode}
              style={{ width: "90%" }}
              onChangeText={value => this.props.actions.setCountryCode(value)}
              keyboardType={"numeric"}
            />
          </View>
          <View style={{ width: "75%" }}>
            <TextInput
              autoFocus
              onChangeText={value => this.phoneNumberInputChange(value)}
              // label={"Phone number"}
              label={"Телефонный номер"}
              value={phoneNumber}
              keyboardType={"phone-pad"}
              onSubmitEditing={this.signIn}
              error={hasErrors}
            />
          </View>
        </View>
        <View>
          {hasErrors ? (
            <HelperText
              style={{ width: "75%", alignSelf: "flex-end" }}
              type="error"
              visible={hasErrors}
            >
              {/* Please enter a valid phone number. */}
              Введите пожалуйста корректный телефонный номер.
            </HelperText>
          ) : (
            <HelperText type="info" visible={!hasErrors}>
              {/* We will send an SMS with a confirmation code to your phone number. */}
              Мы отправим СМС с кодом подтверждения на Ваш телефонный номер.
            </HelperText>
          )}
        </View>
      </View>
    );
  }

  render() {
    const { user } = this.props.user;
    // const { message } = this.props.message;
    const { phoneNumber, countryCode, isFetching } = this.props.codeRequest;
    const {
      theme: {
        colors: { background },
      },
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: background }}>
        {!user && this.renderPhoneNumberInput()}
        {/* {__DEV__ && <Message message={message} />} */}
        {phoneNumber.length > 0 &&
          countryCode.length > 0 &&
          this.props.navigation.state.routeName === "PhoneAuth" && (
            <ForwardFAB loading={isFetching} onPress={this.signIn} />
          )}
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    // message: state.message,
    user: state.user,
    codeRequest: state.codeRequest,
    codeCheck: state.codeCheck
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PhoneAuthDefault)
);
