import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Image,
  Text,
  TouchableNativeFeedback
} from "react-native";
import {
  Card,
  CardContent,
  CardCover,
  Title,
  Paragraph,
  withTheme,
  TouchableRipple
} from "react-native-paper";
import { newsRef } from "../firebase";

class NewsList extends PureComponent {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
  }

  state = {};

  componentDidMount() {
    this.unsubscribe = newsRef.onSnapshot(this.onCollectionUpdate);
  }

  onCollectionUpdate = querySnapshot => {
    const list = [];
    querySnapshot.forEach(doc => {
      const data = doc.data();
      list.push({
        key: doc.id,
        ...data
      });
    });
    this.props.actions.setNewsList(list);
  };

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  cardPressed = news => {
    this.props.navigation.navigate("NewsItem", { news });
  };

  newsCard = news => {
    const { key, name, pictureUrl } = news;
    // console.log({dgdsg: TouchableNativeFeedback.canUseNativeForeground() })

    return (
      // <TouchableNativeFeedback
      //   key={key}
      //   background={TouchableNativeFeedback.Ripple("red")}
      //   style={{ margin: 20, padding: 20 }}
      // >
        <Card key={key}>
          <CardContent>
            <Title>{name}</Title>
            <Paragraph />
          </CardContent>
          {/* <TouchableNativeFeedback
            // background={TouchableNativeFeedback.Ripple("red")}
            useForeground={true}
          > */}
            <TouchableOpacity onPress={() => this.cardPressed(news)}>
            <CardCover source={{ uri: pictureUrl }} />
          {/* </TouchableNativeFeedback> */}
          </TouchableOpacity>
        </Card>
      // </TouchableNativeFeedback>
    );
  };

  render() {
    const { list, isFetching } = this.props.news;
    const listItems = list.map(news => this.newsCard(news));
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    return (
      <ScrollView style={{ backgroundColor: background }}>
        <View style={styles.container}>
          {isFetching && <ActivityIndicator />}
          {listItems}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  defaultView: {
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#77dd77",
    flex: 1
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    news: state.news
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(NewsList)
);
