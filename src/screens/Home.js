import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import { View, StyleSheet, ScrollView } from "react-native";
import {
  Text,
  Button,
  Card,
  CardActions,
  CardContent,
  CardCover,
  Title,
  Paragraph,
  ListSection,
  ListItem,
  withTheme
} from "react-native-paper";
import plainFirebase from "react-native-firebase";
import {
  app as firebase,
  notificationsChannelId,
  notification
} from "../firebase";

class Home extends PureComponent {
  signOut = () => {
    this.props.actions.resetUser().then(() => {
      this.props.actions.resetPhone();
      this.props.actions.resetMessage();
      this.props.actions.resetCode();
      this.props.navigation.navigate("Auth");
    });
  };

  resetName = () => {
    const { user } = this.props.user;

    this.props.actions
      .setUserName(user, null)
      .then(user => this.props.actions.setMessage("Name Set!"))
      .then(() => this.props.navigation.navigate("Home"))
      .catch(error =>
        this.props.actions.setMessage(`Name Set Error: ${error.message}`)
      );
  };

  createNotification = () => {
    const { notificationId, title, body, data } = {
      notificationId: "0:1533467688160960%259d5329259d5329"
    };
    const myNotification = new notification()
      .setNotificationId(notificationId)
      .setTitle(title ? title : "some title")
      .setBody(body ? body : "some body")
      .setData(
        data
          ? data
          : {
              key1: "value1",
              key2: "value2"
            }
      )
      .android.setChannelId(notificationsChannelId)
      .android.setSmallIcon("ic_launcher");

    firebase.notifications().displayNotification(myNotification);

    // console.log(myNotification);
  };

  render() {
    const { user } = this.props.user;
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    return (
      <ScrollView style={{ backgroundColor: background }}>
        <View style={styles.container}>
          <Card>
            <CardContent>
              <Title>Card title</Title>
              <Paragraph>Card content</Paragraph>
            </CardContent>
            <CardCover source={{ uri: "https://picsum.photos/700" }} />
            <CardActions>
              <Button>Cancel</Button>
              <Button>Ok</Button>
              <Button onPress={this.createNotification}>
                Create Notification
              </Button>
            </CardActions>
          </Card>
          {user && (
            <Card>
              <CardContent>
                <Title>User Info</Title>
                <ListSection>
                  <ListItem title={user.uid} icon="adb" />
                  <ListItem title={user.phoneNumber} icon="phone" />
                  <ListItem title={user.displayName} icon="account-circle" />
                </ListSection>
                <Paragraph>
                  <Text>{JSON.stringify(user)}</Text>
                </Paragraph>
              </CardContent>
              <CardActions>
                <Button onPress={this.resetName}>Reset Name</Button>
                <Button onPress={this.signOut}>Sign Out</Button>
              </CardActions>
            </Card>
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  defaultView: {
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#77dd77",
    flex: 1
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    message: state.message,
    user: state.user,
    codeRequest: state.codeRequest,
    codeCheck: state.codeCheck
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)
);
