import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionCreators from "../state/actions";
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Text,
  Button,
  Card,
  CardActions,
  CardContent,
  CardCover,
  Title,
  Paragraph,
  TouchableRipple,
  withTheme
} from "react-native-paper";
import { hookahRef } from "../firebase";

class HookahList extends PureComponent {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
  }

  componentDidMount() {
    this.unsubscribe = hookahRef.onSnapshot(this.onCollectionUpdate);
  }

  onCollectionUpdate = querySnapshot => {
    // console.log(querySnapshot, "onCollectionUpdate");
    const list = [];
    querySnapshot.forEach(doc => {
      const data = doc.data();
      list.push({
        key: doc.id,
        ...data
      });
    });
    this.props.actions.setHookahList(list);
  };

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  cardPressed = hookah => {
    // console.log("Pressed");
    this.props.navigation.navigate("HookahItem", {hookah});
    // console.log("PressedTTTT");
  };

  // buttonPressed = () => {
  //   console.log("PressedD");
  //   this.props.actions.fetchHookas();
  // };

  hookahCard = hookah => {
    const { key, name, description, pictureUrl } = hookah;
    return (
      <Card key={key}>
        <CardContent>
          <Title>{name}</Title>
          <Paragraph>
            {/* {description.length > 100
              ? `${description.substring(0, 100)}...`
              : description} */}
          </Paragraph>
        </CardContent>
        <TouchableOpacity onPress={() => this.cardPressed(hookah)}>
          <CardCover source={{ uri: pictureUrl }} />
        </TouchableOpacity>
        {/* <CardActions>
          <Button onPress={() => this.buttonPressed()}>Reload</Button>
        </CardActions> */}
      </Card>
    );
  };

  render() {
    const { list, isFetching } = this.props.hookah;
    const listItems = list.map(hookah => this.hookahCard(hookah));
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    return (
      <ScrollView style={{ backgroundColor: background }}>
        <View style={styles.container}>
          {isFetching && <ActivityIndicator />}
          {listItems}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  defaultView: {
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#77dd77",
    flex: 1
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    hookah: state.hookah,
    // message: state.message,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actionCreators, dispatch) };
};

export default withTheme(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(HookahList)
);
