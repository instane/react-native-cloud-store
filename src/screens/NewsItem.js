import React, { Component, PureComponent } from "react";
import { View, ScrollView, StyleSheet, Dimensions, Image } from "react-native";
import {
  withTheme,
  Card,
  CardCover,
  CardContent,
  Paragraph,
  Text
} from "react-native-paper";

class NewsItem extends PureComponent {
  state = {
    imgWidth: 0,
    imgHeight: 0
  };

  calculateImageWidthHeight = image => {
    Image.getSize(image, (width, height) => {
      const screenWidth = Dimensions.get("window").width;
      const scaleFactor = width / screenWidth;
      const imageHeight = height / scaleFactor;
      this.setState({ imgWidth: screenWidth, imgHeight: imageHeight });
    });
  }

  componentDidMount() {
    const { navigation } = this.props;
    const news = navigation.getParam("news", {});
    const { pictureUrl } = news;

    this.calculateImageWidthHeight(pictureUrl);
  }

  render() {
    const {
      theme: {
        colors: { background }
      },
      navigation
    } = this.props;
    const news = navigation.getParam("news", {});
    const { description, pictureUrl } = news;
    const { imgWidth, imgHeight } = this.state;

    return (
      <ScrollView style={{ backgroundColor: background }}>
        <View style={{ flex: 1 }}>
          <Card>
            <CardCover
              style={{ width: imgWidth, height: imgHeight }}
              source={{ uri: pictureUrl }}
            />
            {description.length > 0 && (
              <CardContent>
                <Paragraph>
                  <Text>{description}</Text>
                </Paragraph>
              </CardContent>
            )}
          </Card>
        </View>
      </ScrollView>
    );
  }
}

export default withTheme(NewsItem);
