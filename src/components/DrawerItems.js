import React, { Component, PureComponent } from "react";
import { View, StyleSheet, Platform } from "react-native";
import {
  DrawerItem,
  DrawerSection,
  withTheme,
  Colors
} from "react-native-paper";
import { withNavigation } from "react-navigation";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const DrawerItemsData = [
  // { label: "Главная", icon: "star", path: "HomeStack", key: 0 },
  {
    label: "Новости",
    // icon: <Icon name="newspaper" size="12" color="#000000" />,
    icon: "star",
    path: "NewsStack",
    key: 3
  },
  { label: "Кальяны", icon: "star", path: "HookahStack", key: 1 },
  { label: "Табаки", icon: "star", path: "TobaccoStack", key: 2 },
  
];

class DrawerItems extends PureComponent {
  state = {
    open: false,
    drawerItemIndex: 0
  };

  _setDrawerItem = index => {
    this.setState({ drawerItemIndex: index });
    this.props.navigation.navigate(DrawerItemsData[index].path);
  };

  render() {
    const themeColor = this.props.theme.colors.paper;
    return (
      <View style={[styles.drawerContent, { backgroundColor: themeColor }]}>
        {/* <DrawerSection title="Subheader"> */}
        <DrawerSection>
          {DrawerItemsData.map((props, index) => (
            <DrawerItem
              {...props}
              key={props.key}
              color={props.colored === true ? Colors.tealA200 : undefined}
              active={this.state.drawerItemIndex === index}
              onPress={() => this._setDrawerItem(index)}
            />
          ))}
        </DrawerSection>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 25 : 22
  }
});

export default withNavigation(withTheme(DrawerItems));
