import React, { PureComponent } from "react";
import { FABGroup } from "react-native-paper";
import { ActivityIndicator } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class ForwardFAB extends PureComponent {
  icon = (size, color) => {
    if (this.props.loading ? this.props.loading : false) {
      return <ActivityIndicator size={size} color={color} />;
    }
    return <Icon name="arrow-forward" size={size} color={color} />;
  };

  render() {
    const fakeState = false;
    const onPress = this.props.onPress ? this.props.onPress : () => {};

    return (
      <FABGroup
        open={fakeState}
        onStateChange={() => {}}
        icon={({ size, color }) => this.icon(size, color)}
        actions={[]}
        onPress={onPress}
      />
    );
  }
}
