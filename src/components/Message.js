import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Message = props => {
  const renderMessage = () => {
    return <Text style={styles.text}>{props.message}</Text>;
  };
  return props.message.length > 0 ? renderMessage() : <View />;
};

const styles = StyleSheet.create({
  text: {
    padding: 5,
    backgroundColor: "#000",
    color: "#fff"
  }
});

export default Message;
