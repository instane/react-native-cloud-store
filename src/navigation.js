import React from "react";
import {
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator
} from "react-navigation";
import {
  Toolbar,
  ToolbarContent,
  ToolbarAction,
  ToolbarBackAction
} from "react-native-paper";
import PhoneAuth from "./screens/PhoneAuth";
import PhoneAuthConfirmation from "./screens/PhoneAuthConfirmation";
import SetName from "./screens/SetName";
// import Home from "./screens/Home";
import AuthLoading from "./screens/AuthLoading";
import NewsList from "./screens/NewsList";
import NewsItem from "./screens/NewsItem";
import HookahList from "./screens/HookahList";
import HookahItem from "./screens/HookahItem";
import TobaccoList from "./screens/TobaccoList";
import TobaccoItem from "./screens/TobaccoItem";
import DrawerItems from "./components/DrawerItems";
import Icon from "react-native-vector-icons/MaterialIcons";
import IconCommunity from "react-native-vector-icons/MaterialCommunityIcons";

const HookahStack = createStackNavigator(
  {
    HookahList: {
      screen: HookahList,
      navigationOptions: ({ navigation }) => ({
        title: "HookahList"
      })
    },
    HookahItem: {
      screen: HookahItem,
      navigationOptions: ({ navigation }) => ({
        title: "HookahItem"
      })
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      header: (
        <Toolbar>
          {navigation.state.routeName === "HookahList" && (
            <ToolbarAction
              icon="menu"
              onPress={() => navigation.openDrawer()}
            />
          )}
          {navigation.state.routeName === "HookahList" && (
            <ToolbarContent title="Кальяны" />
          )}
          {navigation.state.routeName === "HookahItem" && (
            <ToolbarAction
              icon="arrow-back"
              onPress={() => navigation.goBack()}
            />
          )}
          {navigation.state.routeName === "HookahItem" && (
            <ToolbarContent title={navigation.getParam("hookah", {name: ""}).name} />
          )}
        </Toolbar>
      )
    })
  }
);

const TobaccoStack = createStackNavigator(
  {
    TobaccoList: {
      screen: TobaccoList,
      navigationOptions: ({ navigation }) => ({
        title: "TobaccoList"
      })
    },
    TobaccoItem: {
      screen: TobaccoItem,
      navigationOptions: ({ navigation }) => ({
        title: "TobaccoItem"
      })
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      header: (
        <Toolbar>
          {navigation.state.routeName === "TobaccoList" && (
            <ToolbarAction
              icon="menu"
              onPress={() => navigation.openDrawer()}
            />
          )}
          {navigation.state.routeName === "TobaccoList" && (
            <ToolbarContent title="Табаки" />
          )}
          {navigation.state.routeName === "TobaccoItem" && (
            <ToolbarAction
              icon="arrow-back"
              onPress={() => navigation.goBack()}
            />
          )}
          {navigation.state.routeName === "TobaccoItem" && (
            <ToolbarContent title={navigation.getParam("tobacco", {name: ""}).name} />
          )}
        </Toolbar>
      )
    })
  }
);

const NewsStack = createStackNavigator(
  {
    NewsList: {
      screen: NewsList,
      navigationOptions: ({ navigation }) => ({
        title: "NewsList"
      })
    },
    NewsItem: {
      screen: NewsItem,
      navigationOptions: ({ navigation }) => ({
        title: "NewsItem"
      })
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      header: (
        <Toolbar>
          {navigation.state.routeName === "NewsList" && (
            <ToolbarAction
              icon="menu"
              onPress={() => navigation.openDrawer()}
            />
          )}
          {navigation.state.routeName === "NewsList" && (
            <ToolbarContent title="Новости" />
          )}
          {navigation.state.routeName === "NewsItem" && (
            <ToolbarAction
              icon="arrow-back"
              onPress={() => navigation.goBack()}
            />
          )}
          {navigation.state.routeName === "NewsItem" && (
            <ToolbarContent title={navigation.getParam("news", {name: ""}).name} />
          )}
        </Toolbar>
      )
    })
  }
);

// const HomeStack = createStackNavigator(
//   {
//     Home: {
//       screen: Home
//     }
//   },
//   {
//     navigationOptions: ({ navigation }) => ({
//       header: (
//         <Toolbar>
//           <ToolbarAction icon="menu" onPress={() => navigation.openDrawer()} />
//           <ToolbarContent title="Home" />
//         </Toolbar>
//       )
//     })
//   }
// );

const AppStack = createDrawerNavigator(
  {
    // HomeStack: {
    //   screen: HomeStack,
    //   navigationOptions: ({ navigation }) => ({
    //     drawerLabel: "Home",
    //     drawerIcon: ({ tintColor }) => <Icon name="add" size={25} />
    //   })
    // },
    NewsStack: {
      screen: NewsStack,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "News",
        drawerIcon: ({ tintColor }) => (
          <IconCommunity name="newspaper" size={25} />
        )
      })
    },
    HookahStack: {
      screen: HookahStack,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "Hookah",
        drawerIcon: ({ tintColor }) => <Icon name="add" size={25} />
      })
    },
    TobaccoStack: {
      screen: TobaccoStack,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "Tobacco",
        drawerIcon: ({ tintColor }) => <Icon name="add" size={25} />
      })
    },
  },
  {
    contentComponent: () => <DrawerItems />
  }
);

const AuthStack = createStackNavigator({
  PhoneAuth: {
    screen: PhoneAuth,
    navigationOptions: ({ navigation }) => ({
      header: (
        <Toolbar>
          <ToolbarContent title="Ваш Телефон" />
        </Toolbar>
      )
    })
  },
  PhoneAuthConfirmation: {
    screen: PhoneAuthConfirmation,
    navigationOptions: ({ navigation }) => ({
      header: (
        <Toolbar>
          <ToolbarBackAction onPress={() => navigation.goBack()} />
          <ToolbarContent title="Код Подтверждения" />
        </Toolbar>
      )
    })
  }
});

const ProfileInitStack = createStackNavigator({
  SetName: {
    screen: SetName,
    navigationOptions: ({ navigation }) => ({
      header: (
        <Toolbar>
          <ToolbarContent title="Ваше Имя" />
        </Toolbar>
      )
    })
  }
});

const RootStack = createSwitchNavigator(
  {
    AuthLoading,
    App: AppStack,
    Auth: AuthStack,
    ProfileInit: ProfileInitStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);

export default RootStack;
