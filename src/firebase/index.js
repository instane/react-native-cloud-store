import firebase from "react-native-firebase";

const config = {
  clientId:
    "649204349502-okv472dnipm0vikkuio0028uu52se8br.apps.googleusercontent.com",
  appId: "1:649204349502:android:8d4e41f333ad27fa",
  apiKey: "AIzaSyD_o3u53-mezkD4vZv44IBfs0VY5OFHjKE",
  databaseURL: "https://cloud-331b2.firebaseio.com",
  storageBucket: "cloud-331b2.appspot.com",
  messagingSenderId: "649204349502",
  projectId: "cloud-331b2",

  // enable persistence by adding the below flag
  persistence: true
};

const app = firebase.initializeApp(config, "cloud");

const maxAndroidNotificationPriority =
  firebase.notifications.Android.Priority.Max;

const notification = firebase.notifications.Notification;
const notificationsChannelId = "cloud-channel";
const notificationsChannel = new firebase.notifications.Android.Channel(
  "cloud-channel",
  "Сloud Channel",
  firebase.notifications.Android.Importance.Max
).setDescription("Cloud Store notifications channel");

app.notifications().android.createChannel(notificationsChannel);
const hookahRef = app.firestore().collection("hookah");
const tobaccoRef = app.firestore().collection("tobacco");
const newsRef = app.firestore().collection("news");

export {
  hookahRef,
  tobaccoRef,
  newsRef,
  app,
  notificationsChannel,
  notificationsChannelId,
  notification,
  maxAndroidNotificationPriority
};
